<?php
/*
Plugin Name: 3X Campaing Redirect
Plugin URI: https://3xweb.site
Description: Permite a configuração de redirecionamento com parametros personalizados para testes A-B.
Version: 1.0
Author: Douglas de Araújo
Author URI: https://3web.site
License: Kopimi
*/

// primeiro, se eu estiver na página de admin não vou fazer nada (y)
if (is_admin()) return;
// depois verifico se na url requerida existe o endereço de nosso interesse
if (strpos($_SERVER["REQUEST_URI"], 'ipd01-captura-0') !== false) {
    // se não houver nenhum parametro, o usuário chegou até aqui diretamente
    // então vou preencher alguns parametros para ele ou seguir com os parametros recebidos 
    $parametros = ( !empty($_SERVER['QUERY_STRING']) ) 
        ? $_SERVER['QUERY_STRING']  
        : 'utm_source=direto&utm_medium=neutro&utm_campaign=tex-ipd01-cap-direto-neutro-x-x-x';
    // finalmente defino as páginas que vão ser redirecionadas
    $paginas = array( 'ipd01-captura-b','ipd01-captura-d','ipd01-captura-e','ipd01-captura-g','ipd01-captura-h','ipd01-captura-i');
    // escolho uma página aleatória sorteandos os índices do array
    $pagina_aletoria = array_rand($paginas, 1);
    $versao = explode('-', $paginas[$pagina_aletoria]);
    // faço o redirecionamento repassando os parâmetros recebidos para a página sorteada
    header('Location: https://imersaoprofissoesdigitais.com.br/'.$paginas[$pagina_aletoria].'/?'.$parametros.'&versao=lp-'.$versao[2]);
    // finaliza a execução
    exit();
}